package br.test.sergio;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;

import io.restassured.RestAssured;

public class VerbosTest {
	

	@BeforeClass
	public static void set() {
		RestAssured.baseURI = "https://reqres.in/api";
		
	}
	

	
	
	@Test
	public void ValidarMetPost() {
		
		Map<String, Object> params = new HashMap<String, Object> ();
		params.put("name", "Usu�rios Testes");
		params.put("age", "33");

		given()
		.log().all()
		.contentType("application/json")
		.body(params)
		.when()
			.post("/api/users")
			
		.then()
			.log().all()
			.statusCode(201)
			.body("id", is(notNullValue()))
			.body("name", containsString("Usu�rios Testes"))
			;

	}
	
	
	
	@Test
	public void ValidarMetGetSingle() {

		given()
		.when()
			.get("/users/3")
		.then()
			.statusCode(200)
			.body("data.id", is(3))
			.body("data.first_name", containsString("Emma"))
			.body("data.last_name", containsString("Wong"))
			;

	}
	
	@Test
	public void ValidarListUsers() {

		given()
		.when()
			.get("https://reqres.in/api/unknown")
		.then()
			.statusCode(200)
			.body(is(not(nullValue())))
			.body("data.id", hasSize(6));

		
	}
	
	
	@Test
	public void ValidarDeletarUsuarioTest() {
		given()
		.log().all()
		.when()
			.delete("/users/2")
		.then()
		.log().all()
		.statusCode(204)
		
		   ;
		
		
	}
	
	@Test
	public void ValidarAlterarUsuarioTest() {
		
		
	
		
		given()
		.log().all()
		.contentType("application/json")
		.body("{\"email\": \"sergio.bluth@reqres.in\",\"first_name\": \"sergio\",\"last_name\": \"rodrigues\",	\"avatar\": \"https://reqres.in/img/faces/1-image.jpg\"}")

		
		
		.when()
			.put("/api/users/3")
		.then()
		.log().all()
		.statusCode(200)
		  
			.body("first_name", is("sergio"))
		
		   ;
		
		
	}
	

	
	

	
	
	
	
	

	




}
